import Things from './Things';
import NewThing from './NewThing';

function App() {
  return (
    <div className='container'>
      <h1>Sup</h1>
      <hr />

      <div className='row'>
        <div className='col'>
          <Things />
        </div>
        <div className='col'>
          <NewThing />
        </div>
      </div>
    </div>
  );
}

export default App;
