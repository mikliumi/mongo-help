import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const thingsApi = createApi({
  reducerPath: 'thingsApi',
  baseQuery: fetchBaseQuery({ baseUrl: `${process.env.REACT_APP_SAMPLE_SERVICE_API_HOST}/api/things` }),
  endpoints: (builder) => ({
    deleteThing: builder.mutation({
      query: (id) => ({
        url: id,
        method: 'DELETE'
      }),
      invalidatesTags: (result, error, {id}) => [{type: 'Things', id}]
    }),
    createThing: builder.mutation({
      query: (body) => ({
        url: '',
        method: 'POST',
        body
      }),
      invalidatesTags: [{ type: 'Things', id: 'LIST' }]
    }),
    getThings: builder.query({
        query: () => '',
        transformResponse: (response, meta, arg) => response.things,
        providesTags: (result) => [
          ...result.map(({id}) => ({type: 'Things', id})),
          { type: 'Things', id: 'LIST' }
        ]
    })
  }),
})

export const { useCreateThingMutation, useDeleteThingMutation, useGetThingsQuery } = thingsApi;
