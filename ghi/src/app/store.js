import { configureStore } from '@reduxjs/toolkit'
import newThingReducer from '../features/thing/newThingSlice'
import { setupListeners } from '@reduxjs/toolkit/query'
import { thingsApi } from '../services/things'

export const store = configureStore({
  reducer: {
    newThing: newThingReducer,
    [thingsApi.reducerPath]: thingsApi.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thingsApi.middleware)
})

setupListeners(store.dispatch)
